import express, { Router } from "express";
import { ENDPOINTS } from "../helpers/endpoints";
import GlobalError from "../utils/GlobalError";

const otherRouter: Router = express.Router();

otherRouter.get(ENDPOINTS.ROUTE_ITEMS_GET, (req, res) => {
  res.status(200).send("Welcome to SMS FS Challenge - Backend API");
});
otherRouter.get(ENDPOINTS.ROUTE_OTHER, (req, res, next) => {
  next(new GlobalError(`Can't find ${req.originalUrl} on server`, 404, 60));
});

export default otherRouter;
