import express, { Router } from "express";
import { ENDPOINTS } from "../helpers/endpoints";
import {
  deleteItemByID,
  getAllItems,
  getItemByID,
  createNewItem,
  updateItem,
} from "../controllers/itemsController";

const itemsRouter: Router = express.Router();

itemsRouter.get(ENDPOINTS.ROUTE_ITEMS_GET, getAllItems);
itemsRouter.get(ENDPOINTS.ROUTE_ITEMS_GET_BY_ID, getItemByID);
itemsRouter.delete(ENDPOINTS.ROUTE_ITEMS_DELETE, deleteItemByID);
itemsRouter.post(ENDPOINTS.ROUTE_ITEMS_POST, createNewItem);
itemsRouter.put(ENDPOINTS.ROUTE_ITEMS_PUT, updateItem);

export default itemsRouter;
