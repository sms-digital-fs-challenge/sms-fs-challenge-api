import { Router } from "express";
import itemsRouter from "./itemsRoutes";
import express from "express";
import { ENDPOINTS } from "../helpers/endpoints";
import otherRouter from "./otherRoutes";
const routes: Router = express.Router();

routes.use(ENDPOINTS.ROUTE_ITEMS, itemsRouter);
routes.use(ENDPOINTS.ROUTE_INDEX, otherRouter);

export default routes;
