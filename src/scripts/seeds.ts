import dotenv from "dotenv";
import mongoose from "mongoose";
import { itemEncoder } from "../helpers/types";

dotenv.config();
export interface IData {
  id: number;
  city: string;
  start_date: Date;
  end_date: Date;
  price: number;
  status: string;
  color: string;
}
/**
 * Parsing the json in dataJson object
 */
const dataJson = require("../data/data.json") as Array<IData>;

/**
 *
 * @param model of type Mongoose model with any as generic type
 *
 * First, check the model to seed and then create an array of IData by encoding the json items
 * We check in MongoDB and we seed only if the the data is missing
 */
async function seed(model: mongoose.Model<any>) {
  const modelName = model.modelName;
  const modelSchema = model.schema;
  const modelToSeed = mongoose.model(modelName, modelSchema);
  const data = dataJson.map((item: IData) => itemEncoder(item));
  const seedDB = async function () {
    const documents = await modelToSeed.find();
    if (documents.length == 0) {
      await modelToSeed.deleteMany({});
      await modelToSeed.insertMany(data);
    }
  };

  seedDB();
}

export default seed;
