import dotenv from "dotenv";
import mongoose from "mongoose";
import { DBCONSTANTS } from "../helpers/constants";
dotenv.config();
const { MONGO_URI } = process.env;
/**
 * Function to connect to mongoDB cluster and return a message
 * indicating whether the connection is established or failed
 */
async function connexion() {
  mongoose
    .connect(MONGO_URI || "")
    .then(() => {
      console.log(DBCONSTANTS.DB_CONNECTION_SUCCESS);
    })
    .catch((err) => {
      console.error(`${DBCONSTANTS.DB_CONNECTION_FAILURE} : ${err}`);
    });
}
export default connexion;
