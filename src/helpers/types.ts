import mongoose from "mongoose";
import { IData } from "../scripts/seeds";

export interface IDataItem {
  _id?: mongoose.Types.ObjectId;
  id: number;
  city: string;
  startDate: Date;
  endDate: Date;
  price: number;
  status: string;
  color: string;
}
/**
 * Encoder used to decorate Json data and adapt it with mongoose model
 */
export function itemEncoder(data: IData): IDataItem {
  return {
    id: data.id,
    city: data.city,
    startDate: data.start_date,
    endDate: data.end_date,
    price: data.price,
    status: data.status,
    color: data.color,
  };
}
