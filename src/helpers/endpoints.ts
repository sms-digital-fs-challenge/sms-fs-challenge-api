export const ENDPOINTS = {
  ROUTE_INDEX: "/",
  ROUTE_OTHER: "*",
  ROUTE_ITEMS: "/items",
  ROUTE_ITEMS_GET: "/",
  ROUTE_ITEMS_GET_BY_ID: "/:id",
  ROUTE_ITEMS_PUT: "/:id",
  ROUTE_ITEMS_POST: "/",
  ROUTE_ITEMS_DELETE: "/:id",
};
