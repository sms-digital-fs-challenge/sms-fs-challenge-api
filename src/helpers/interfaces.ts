import { HttpStatusCode } from "./enums/enumStatus";
export interface IResponse<T> {
  data?: T[] | T;
  count?: number;
  errorCode?: number;
  message?: string;
  status?: string;
  statusCode: HttpStatusCode;
}
