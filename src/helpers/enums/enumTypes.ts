/**
 * Numbers for response messages
 * We use those numbers for optimized time comparison in the switch case
 */
export enum ResponseMessageEnums {
  METHOD_POST = 1,
  METHOD_GET = 2,
  METHOD_PUT = 3,
  METHOD_DLETE = 4,
  DATA_TYPE_OBJECT = 5,
  DATA_TYPE_ARRAY = 6,
}
