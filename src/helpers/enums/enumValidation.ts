/**
 * Codes for errors
 * We update this list depending on our need
 */
export enum ErrorCode {
  DATANOTFOUND = 1,
  ITEMNOTFOUND = 2,
  VALIDATIONERROR = 3,
  CASTINGERROR = 4,
  EMPTYDATA = 5,
}
