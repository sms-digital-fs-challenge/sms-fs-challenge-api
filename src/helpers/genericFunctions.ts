import moment from "moment";
import { ResponseMessageEnums } from "./enums/enumTypes";
/**
 *
 * @param model Mongoose model to retrieve model name
 * @param method HTTP method to customize the response message. Defined as number for less time comparison
 * @param dataType The type of the data so we can add 's' to the model name in case we have an array
 * @returns Formatted response message
 */
export function responseMessageGenerator(
  model: any,
  method:
    | ResponseMessageEnums.METHOD_POST
    | ResponseMessageEnums.METHOD_GET
    | ResponseMessageEnums.METHOD_PUT
    | ResponseMessageEnums.METHOD_DLETE,
  dataType?:
    | ResponseMessageEnums.DATA_TYPE_ARRAY
    | ResponseMessageEnums.DATA_TYPE_OBJECT
): string {
  let action: string = "retrieved";
  switch (method) {
    case ResponseMessageEnums.METHOD_POST:
      action = "posted";
      break;
    case ResponseMessageEnums.METHOD_PUT:
      action = "updated";
      break;
    case ResponseMessageEnums.METHOD_DLETE:
      action = "deleted";
      break;
  }
  let modelName: string = model.modelName;
  let addedS: string =
    dataType === ResponseMessageEnums.DATA_TYPE_ARRAY ? "s" : "";
  let resultMessage: string = modelName + addedS + " has been " + action;
  return (
    resultMessage.toLowerCase().charAt(0).toUpperCase() + resultMessage.slice(1)
  );
}
export function isMongooseId(id: string | undefined) {
  return id ? id.match(/^[0-9a-fA-F]{24}$/) : false;
}
