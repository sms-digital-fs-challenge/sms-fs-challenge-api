import mongoose from "mongoose";
import { Schema, model } from "mongoose";
import { IDataItem } from "../helpers/types";
/**
 * Model schema following the json data
 * the _id is used to store the document id from mongoDB
 */
const dataItemSchema = new Schema<IDataItem>({
  _id: mongoose.Schema.Types.ObjectId,
  id: { type: Number, unique: true },
  city: { type: String, required: [true, "City is required"] },
  startDate: { type: Date, required: [true, "Start date is required"] },
  endDate: { type: Date, required: [true, "End date is required"] },
  price: { type: Number, required: [true, "Price is required"] },
  status: { type: String, required: [true, "Status is required"] },
  color: { type: String, required: [true, "Color is required"] },
});

dataItemSchema.pre("save", async function (next) {
  if (this.isNew) {
    const maxId: any = await dataItemModel.find().sort({ id: -1 }).limit(1);
    this.id = maxId[0].id + 1;
  } else {
    next();
  }
});

const dataItemModel = model<IDataItem>("Item", dataItemSchema);

export default dataItemModel;
