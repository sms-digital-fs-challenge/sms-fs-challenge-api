import { Request, Response } from "express";
import moment from "moment";
import mongoose from "mongoose";
import { HTTPCONSTANTS } from "../helpers/constants";
import {
  ErrorCode,
  HttpStatusCode,
  ResponseMessageEnums,
} from "../helpers/enums";
import {
  isMongooseId,
  responseMessageGenerator,
} from "../helpers/genericFunctions";
import { IResponse } from "../helpers/interfaces";
import { IDataItem } from "../helpers/types";
import dataItemModel from "../models/dataItem";
import APIFeatures from "../utils/apiFeatures";
import catchAsync from "../utils/catchAsync";
import GlobalError from "../utils/GlobalError";

interface IQueryObject<T> {
  startDate?: Object;
  endDate?: Object;
  $and?: Array<T>;
}

/**
 *
 * @param model generic model
 * @returns list of items if they exist or not found response
 */
export const genericGet = function (model: mongoose.Model<any>) {
  return catchAsync(async (req: Request, res: Response, next: any) => {
    let queryObject: IQueryObject<Object> = {};
    const start = req.query.startDate
      ? new Date(req.query.startDate.toString())
      : null;
    const end = req.query.endDate
      ? new Date(req.query.endDate.toString())
      : null;

    if (req.query.startDate && req.query.endDate) {
      queryObject.$and = [
        {
          startDate: { $gte: start },
        },
        { endDate: { $lte: end } },
      ];
    } else if (req.query.startDate) {
      queryObject.startDate = {
        $gte: start,
      };
    } else if (req.query.endDate) {
      queryObject.endDate = {
        $lte: end,
      };
    }

    const query = model.find(queryObject);

    const paginatedQuery = new APIFeatures(query, req.query).paginate().sort();
    const documents = await paginatedQuery.query;
    if (documents.length == 0)
      return next(
        new GlobalError(
          `Empty list of ${model.modelName}`,
          HttpStatusCode.NO_CONTENT,
          ErrorCode.EMPTYDATA
        )
      );
    res.status(HttpStatusCode.OK).send(<IResponse<IDataItem[]>>{
      status: HTTPCONSTANTS.SUCCESS_MESSAGE,
      statusCode: HttpStatusCode.OK,
      message: responseMessageGenerator(
        dataItemModel,
        ResponseMessageEnums.METHOD_GET,
        ResponseMessageEnums.DATA_TYPE_ARRAY
      ),
      count: documents.length,
      data: documents,
    });
  });
};

/**
 *
 * @param model Generic model
 * @returns the item if found or empty response if not
 */
export const genericGetById = function (model: mongoose.Model<any>) {
  return catchAsync(async (req: Request, res: Response, next: any) => {
    let id = req.params.id;
    let key: string = isMongooseId(id) ? "_id" : "id";
    const document = await model.findOne({ [`${key}`]: id });
    if (!document)
      return next(
        new GlobalError(
          `${model.modelName} not found`,
          HttpStatusCode.NOT_FOUND,
          ErrorCode.DATANOTFOUND
        )
      );
    res.status(HttpStatusCode.OK).send(<IResponse<IDataItem>>{
      status: HTTPCONSTANTS.SUCCESS_MESSAGE,
      statusCode: HttpStatusCode.OK,
      message: `${model.modelName} found`,
      data: document,
    });
  });
};

/**
 *
 * @param model Generic model
 * @returns the inserted new item if successful or error if exist
 */
export const genericPost = function (model: mongoose.Model<any>) {
  return catchAsync(async (req: Request, res: Response, next: any) => {
    let document: any = req.body;
    document._id = new mongoose.Types.ObjectId();
    const newDocument = new model(document);
    delete newDocument.id;
    await newDocument.save();
    if (!res.headersSent)
      res.status(HttpStatusCode.CREATED).send(<IResponse<IDataItem>>{
        statusCode: HttpStatusCode.CREATED,
        status: HTTPCONSTANTS.SUCCESS_MESSAGE,
        message: responseMessageGenerator(
          dataItemModel,
          ResponseMessageEnums.METHOD_POST
        ),
        data: newDocument,
      });
  });
};

/**
 *
 * @param model Generic model
 * @returns the deleted item if found or NOT FOUND message
 */
export const genericDeleteById = function (model: mongoose.Model<any>) {
  return catchAsync(async (req: Request, res: Response, next: any) => {
    let id = req.params.id;
    let key: string = isMongooseId(id) ? "_id" : "id";
    const document: any = await model.findOneAndDelete({ [`${key}`]: id });
    if (!document)
      return next(
        new GlobalError(
          `${model.modelName} not found`,
          HttpStatusCode.NOT_FOUND,
          ErrorCode.DATANOTFOUND
        )
      );
    res.status(HttpStatusCode.CREATED).send(<IResponse<IDataItem>>{
      status: HTTPCONSTANTS.SUCCESS_MESSAGE,
      statusCode: HttpStatusCode.OK,
      message: responseMessageGenerator(
        dataItemModel,
        ResponseMessageEnums.METHOD_DLETE
      ),
      data: document,
    });
  });
};

/**
 *
 * @param model generic model
 * @returns an updated item if successful and run validators on values
 *
 * The line 145 to be removed if we accept update on the id field
 */
export const genericPut = function (model: mongoose.Model<any>) {
  return catchAsync(async (req: Request, res: Response, next: any) => {
    let id = req.params.id;
    let document: any = req.body;
    delete document.id;
    let key: string = isMongooseId(id) ? "_id" : "id";
    const documentToUpdate = await model
      .findOneAndUpdate({ [`${key}`]: id }, document, { runValidators: true })
      .exec();
    if (!documentToUpdate)
      return next(
        new GlobalError(
          `${model.modelName} not found`,
          HttpStatusCode.NOT_FOUND,
          ErrorCode.DATANOTFOUND
        )
      );
    if (!res.headersSent)
      res.status(HttpStatusCode.CREATED).send(<IResponse<IDataItem>>{
        status: HTTPCONSTANTS.SUCCESS_MESSAGE,
        statusCode: HttpStatusCode.CREATED,
        message: responseMessageGenerator(
          dataItemModel,
          ResponseMessageEnums.METHOD_PUT
        ),
        data: document,
      });
  });
};
