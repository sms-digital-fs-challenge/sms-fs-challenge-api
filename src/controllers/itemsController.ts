import dataItemModel from "../models/dataItem";
import {
  genericGet,
  genericGetById,
  genericDeleteById,
  genericPost,
  genericPut,
} from "./genericCRUD";
export const getAllItems = genericGet(dataItemModel);
export const getItemByID = genericGetById(dataItemModel);
export const deleteItemByID = genericDeleteById(dataItemModel);
export const createNewItem = genericPost(dataItemModel);
export const updateItem = genericPut(dataItemModel);
