import { Request, Response } from "express";
import { NextFunction } from "express-serve-static-core";
/**
 *
 * @param fn The function to execute
 * @returns the thrown error asresponse rather than a crash in server
 */
const catchAsync = (
  fn: (req: Request, res: Response, next: NextFunction) => Promise<any>
) => {
  return (req: Request, res: Response, next: any) => {
    fn(req, res, next).catch((error: Error) => {
      return next(error);
    });
  };
};
export default catchAsync;
