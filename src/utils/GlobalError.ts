/**
 * Class to return the error as response
 * We used the error code to deeply check the error
 * ( More specific than just a given status code in the frontend )
 */

class CustomError extends Error {
  statusCode!: number;
  status!: string;
  errorId!: number;
  result!: boolean;
  isOperational!: boolean;
  path!: string;
  value!: string;
}
//TODO: Define a generic Map with error's name and message as key/value pair
class GlobalError extends CustomError {
  constructor(message: string, statusCode: number, errorId: number) {
    super(message);
    this.statusCode = statusCode;
    this.errorId = errorId;
    this.status = `${statusCode}`.startsWith("4") ? "FAILED" : "ERROR";
    this.isOperational = true;
    Error.captureStackTrace(this, this.constructor);
  }
}
export default GlobalError;
