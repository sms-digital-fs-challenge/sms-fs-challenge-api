import request from "supertest";
import mongoose from "mongoose";
import app from "../index";
import { ENDPOINTS } from "../src/helpers/endpoints";
import { HTTPCONSTANTS } from "../src/helpers/constants";
import { HttpStatusCode } from "../src/helpers/enums";
import { mockedItem } from "./__mocks__/item";

let itemID = 1000;
const fakeItemID = "x2e8787379fe1208f766079x";
const wrongItemID = 2000;
jest.setTimeout(30000);

beforeAll((done) => {
  mongoose.connection.on("connected", done);
});

describe("DB Connection", () => {
  it("Should connect to MongoDB", (done) => {
    mongoose.connection.readyState === 1 ? done() : done(false);
  });
});

describe("GET Items list", () => {
  it("Should returns code 200 with items list in data", async () => {
    const response = await request(app).get(ENDPOINTS.ROUTE_ITEMS);
    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
  });
});

describe("POST item", () => {
  it("should throw not acceptable error if the body or some fields are missing", async () => {
    const response = await request(app).post(ENDPOINTS.ROUTE_ITEMS);
    expect(response.statusCode).toEqual(HttpStatusCode.NOT_ACCEPTABLE);
    expect(response.body.result).toBe(false);
  });
  it("should insert new item", async () => {
    const response = await request(app)
      .post(ENDPOINTS.ROUTE_ITEMS)
      .send(mockedItem);
    itemID = response.body.data["id"];
    console.log(itemID);

    expect(response.statusCode).toEqual(HttpStatusCode.CREATED);
  });
});
describe("GET Item by ID", () => {
  it("Should returns code 200 with item object", async () => {
    const response = await request(app).get(
      ENDPOINTS.ROUTE_ITEMS + "/" + itemID
    );
    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeDefined();
    expect(response.body.status).toEqual(HTTPCONSTANTS.SUCCESS_MESSAGE);
    expect(response.body.data).toHaveProperty("id", itemID);
  });
  it("Should returns error code 404", async () => {
    const itemID = "0";

    const response = await request(app).get(
      ENDPOINTS.ROUTE_ITEMS + "/" + itemID
    );
    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeDefined();
    expect(response.body.status).toEqual(HTTPCONSTANTS.FAILED_MESSAGE);
    expect(response.body.data).toBeUndefined();
  });
});

describe("PUT item", () => {
  it("should throw NOT Acceptable error if itemID is wrong", async () => {
    const response = await request(app).put(
      ENDPOINTS.ROUTE_ITEMS + "/" + fakeItemID
    );
    expect(response.statusCode).toEqual(HttpStatusCode.NOT_ACCEPTABLE);
    expect(response.body.result).toBe(false);
    expect(response.body.status).toEqual(HTTPCONSTANTS.FAILED_MESSAGE);
    expect(response.body.message).toEqual(`Invalid id : ${fakeItemID}`);
  });
  it("should throw NOT FOUND error if item not found", async () => {
    const response = await request(app).put(
      ENDPOINTS.ROUTE_ITEMS + "/" + wrongItemID
    );
    expect(response.statusCode).toEqual(HttpStatusCode.NOT_FOUND);
    expect(response.body.result).toBe(false);
    expect(response.body.status).toEqual(HTTPCONSTANTS.FAILED_MESSAGE);
  });
  it("should update item", async () => {
    const response = await request(app)
      .put(ENDPOINTS.ROUTE_ITEMS + "/" + itemID)
      .send(mockedItem);
    expect(response.statusCode).toEqual(HttpStatusCode.CREATED);
  });
});
describe("DELETE item", () => {
  it("should throw NOT Acceptable error if itemID is wrong", async () => {
    const response = await request(app).delete(
      ENDPOINTS.ROUTE_ITEMS + "/" + fakeItemID
    );
    expect(response.statusCode).toEqual(HttpStatusCode.NOT_ACCEPTABLE);
    expect(response.body.result).toBe(false);
    expect(response.body.status).toEqual(HTTPCONSTANTS.FAILED_MESSAGE);
    expect(response.body.message).toEqual(`Invalid id : ${fakeItemID}`);
  });
  it("should throw NOT FOUND error if item not found", async () => {
    const response = await request(app).delete(
      ENDPOINTS.ROUTE_ITEMS + "/" + wrongItemID
    );
    expect(response.statusCode).toEqual(HttpStatusCode.NOT_FOUND);
    expect(response.body.result).toBe(false);
    expect(response.body.status).toEqual(HTTPCONSTANTS.FAILED_MESSAGE);
  });
  it("should delete item", async () => {
    const response = await request(app).delete(
      ENDPOINTS.ROUTE_ITEMS + "/" + itemID
    );
    expect(response.statusCode).toEqual(HttpStatusCode.CREATED);
  });
});

afterAll((done) => {
  mongoose.disconnect();
  app.close();
  done();
});
