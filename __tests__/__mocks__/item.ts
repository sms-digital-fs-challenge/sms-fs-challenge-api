export const mockedItem = {
  id: 2000,
  city: "Djerba",
  startDate: "2012-11-29T23:00:00.000Z",
  endDate: "2013-07-12T23:00:00.000Z",
  price: 3.19,
  status: "Weekly",
  color: "#03c2cd",
};
