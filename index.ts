import cors from "cors";
import dotenv from "dotenv";
import express, { Request, Response } from "express";
import http from "http";
import globalErrorHandler from "./src/controllers/errorController";
import connectToDb from "./src/config/db";
import dataItemModel from "./src/models/dataItem";
import routes from "./src/routes";
import seed from "./src/scripts/seeds";

dotenv.config();
const app = express();
const server: http.Server = http.createServer(app);
const { PORT } = process.env;

//initialization
connectToDb();
app.use(cors());
app.use(express.json());

//combined routes
app.use(routes);

//error handling section
app.use(globalErrorHandler);

let serverApp = server.listen(PORT, () => {
  seed(dataItemModel);
  console.log("Listening on port " + PORT);
});

export default serverApp;
