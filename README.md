# Project Title

SMS Digital Challenge for Fullstack position
Backend part

## Authors

- [@Houcem Eddine SANAI](https://tn.linkedin.com/in/houssem-eddine-sanai)

## API Reference

#### Get all items

```http
  GET /items
```

Get items list in a json array

#### Get item

```http
  GET /items/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Id of item to fetch |

#### Add new item

```http
  POST /items
```

| Body                                            | Type     | Description               |
| :---------------------------------------------- | :------- | :------------------------ |
| {city - start date - end date - status - color} | `Object` | **Required**. Item fields |

#### Update item

```http
  PUT /items/${id}
```

| Parameter | Type     | Description                        |
| :-------- | :------- | :--------------------------------- |
| `id`      | `string` | **Required**. Id of item to update |

#### Delete item

```http
  DELETE /items/${id}
```

| Parameter | Type     | Description                        |
| :-------- | :------- | :--------------------------------- |
| `id`      | `string` | **Required**. Id of item to delete |

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`NODE_ENV` to be {development | production }

`PORT` for the express server

`BASE_URL` for exemple **http://localhost**

`MONGO_URI` the mongoDB cluster URL

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/sms-digital-fs-challenge/sms-fs-challenge-api
```

Go to the project directory

```bash
  cd sms-fs-challenge-api
```

Install dependencies

```bash
  yarn
```

Start the server

```bash
  yarn start
```

## Running Tests

To run tests, run the following command

```bash
  yarn test
```

## Docker

To create docker image and run it, please follow these steps:

```bash
  docker build -t {NAME} .
```

```bash
  docker run -p {PORT_INT}:{PORT_EXT} -e PORT={PORT_INT} -e MONGO_URI={MongoClusterURI}  {NAME}
```

## Demo

Demo available here https://smsdigital-api.amitechss.com

## Feedback

If you have any feedback, please reach out to us at sanai.houcem@gmail.com

## License

[MIT](https://choosealicense.com/licenses/mit/)
