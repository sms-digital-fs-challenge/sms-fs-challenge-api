import type { Config } from "jest";

export default async (): Promise<Config> => {
  return {
    preset: "ts-jest",
    displayName: {
      name: "SMS Digital FS Challenge",
      color: "greenBright",
    },
    verbose: true,
    setupFiles: ["dotenv/config"],
    testEnvironment: "node",
    forceExit: true,
    testPathIgnorePatterns: ["<rootDir>/__tests__/__mocks__/"],
  };
};
